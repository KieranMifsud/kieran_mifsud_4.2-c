﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour {

	public bool isFiring;
	public Bullet_Control bullet;
	public float BulletSpeed;
	public float timeBetweenShots;
	private float shotCounter;
	public Transform firePoint;
	//public GameObject bulletExplode;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		//setting variables for the fire rate of my layers
		if (isFiring) {
			shotCounter -= Time.deltaTime;
			if (shotCounter <= 0) {
				shotCounter = timeBetweenShots;
				Bullet_Control newBullet = Instantiate (bullet, firePoint.position, firePoint.rotation) as Bullet_Control;
				newBullet.speed = BulletSpeed;
		}
		} else {
			shotCounter = 0;
		}
	}
}
 