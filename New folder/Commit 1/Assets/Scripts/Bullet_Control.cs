﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Control : MonoBehaviour {
	//this let the user control bullet speed
	public float speed;

	void Start () {
		
	}

	void Update () {
	//makes the bullet travel on one axis from fire point.
		transform.Translate(Vector3.up * speed * Time.deltaTime);
	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Enemy") {
			//destroy(gameObject);
		}
	}
}
