﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunFire : MonoBehaviour {

	public float FireRate = 4;
	public float damage = 5;

	float timeUntilFire = 0;

	void Update() 
	{
		if (Input.GetButtonDown("Fire1") && Time.time > timeUntilFire) {
			timeUntilFire = Time.time + 1 / FireRate;
		}
	}
	void Shoot() {
		Debug.Log ("Shooting");
	}
}
