﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public float moveSpeed;
	private Rigidbody2D myRigidbody;
	Vector3 mousePos;
	Camera cam;
	Rigidbody2D rid;

	public GunController theGun;

	private Vector3 moveInput;
	private Vector3 moveVelocity;

	// Use this for initialization
	void Start () {
		myRigidbody = GetComponent<Rigidbody2D> ();    
		rid = this.GetComponent<Rigidbody2D> ();
		cam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		rotateToCamera ();

		moveInput = new Vector3 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"), 0f);
		moveVelocity = moveInput * moveSpeed;

		if (Input.GetMouseButtonDown (0) || Input.GetKeyDown(KeyCode.Space))
			theGun.isFiring = true;

		if (Input.GetMouseButtonUp (0))
			theGun.isFiring = false;
	}

	void FixedUpdate ()
	{
		myRigidbody.velocity = moveVelocity;
	}

	void rotateToCamera()
	{
		mousePos = cam.ScreenToWorldPoint (new Vector3(Input.mousePosition.x,Input.mousePosition.y,Input.mousePosition.z-cam.transform.position.z));
		rid.transform.eulerAngles = new Vector3 (0, 0, Mathf.Atan2 ((mousePos.y - transform.position.y), (mousePos.x - transform.position.x)) * Mathf.Rad2Deg);
	}


}

  