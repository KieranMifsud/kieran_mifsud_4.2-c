﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour {

	public GameObject pauseUI;//this variable will be used to enable and disable the pauseUI

	private bool paused = false;

	// Use this for initialization
	void Start () {
		pauseUI.SetActive (false);//the UI will be disabled when the game starts
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Pause")) 
		{
			paused = !paused;
		}

		if (paused) 
		{
			pauseUI.SetActive (true);
			Time.timeScale = 0;// stops time when it is paused
		}

		if (!paused) 
		{
			pauseUI.SetActive (false);
			Time.timeScale = 1;//normal time when it is not paused
		}
	}

	public void Resume() 
	{
		paused = false;//when clicked resumes game
	}

	public void Restart()
	{
		Application.LoadLevel (Application.loadedLevel);//restarts game when restart button is pressed
	}


}
