﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerHealth : MonoBehaviour {

	public int CurrentHealth;
	public int MaxHealth = 4;

	// Use this for initialization
	void Start () {
		//this makes the current health equal the max health at the start of the level
		CurrentHealth = MaxHealth;
	}
	
	// Update is called once per frame
	void Update () {
		if (CurrentHealth > MaxHealth) 
		{
			CurrentHealth = MaxHealth;
		}

		if (CurrentHealth <= 0) 
		{
			Die();
		}
	}
	//when player dies the level is restarted
	void Die() 
	{
		Application.LoadLevel (0);
	}

	public void Damage(int dmg) {
		//the health will be reduced by the variable in dmg
		CurrentHealth -= dmg;
	}


}
