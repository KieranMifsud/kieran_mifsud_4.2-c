﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementSpeedPowerup : MonoBehaviour {


	public static float multiplier = 1.6f;//movement speed will be multplied by 1.6
	public float duration = 7f;//time power up lasts
	private GameObject player;

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.CompareTag ("Player")) 
		{
			StartCoroutine( Pickup (other));//will be called to pick up power up
		}
	}


	void Start()
	{
		player = GameObject.Find ("player");
	}

	IEnumerator Pickup(Collider2D player)
	{
		//get the player speed from the playerMovment script
		PlayerMovement speed = player.GetComponentInParent<PlayerMovement> ();
		speed.moveSpeed *= multiplier; //movement speed will be multiplied depending on the multipler

		//disable graphics so the player dosent collide with power up again
		GetComponent<SpriteRenderer> ().enabled = false;
		GetComponent<Collider2D> ().enabled = false;

		yield return new WaitForSeconds (duration);//how many seconds the power up lasts 

	
		speed.moveSpeed /= multiplier;//reverses the effect


		Destroy (gameObject);//destroys power up
	}
}
