﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundVolume : MonoBehaviour {

	public Slider Volume;
	public AudioSource mySound;//the sound that will be used with slider

	
	// Update is called once per frame
	void Update () {
		mySound.volume = Volume.value;//takes the slider value and makes it the same sound.
	}
}
