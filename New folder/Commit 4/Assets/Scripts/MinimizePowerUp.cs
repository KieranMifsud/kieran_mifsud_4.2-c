﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimizePowerUp : MonoBehaviour {

	public float multiplier = 4f;//by how much the normal size will decrese
	public float duration = 6f;//time it lasts before returning to normal

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.CompareTag ("Player")) 
		{
			StartCoroutine( Pickup (other));//will be called to pick up power up
		}
	}

	IEnumerator Pickup(Collider2D player)
	{
		player.transform.localScale /= multiplier;//makes player 4 times smaller

		//disable graphics so the player dosent collide with power up again
		GetComponent<SpriteRenderer> ().enabled = false;
		GetComponent<Collider2D> ().enabled = false;

		yield return new WaitForSeconds (duration);//how many seconds the power up lasts 

		player.transform.localScale *= multiplier;//reverses the effect

		Destroy (gameObject);//destroyes power up
	}
}
