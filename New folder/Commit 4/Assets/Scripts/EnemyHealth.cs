﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

	public int health;
	private int currentHealth;

	// Use this for initialization
	void Start () {
		//the current health of the enemy is equal to the maximun health at the start of the game
		currentHealth = health;	 
	}
	
	// Update is called once per frame
	void Update () {
		if (currentHealth <= 0) //when enemy health gets to o it will be destroyed
		{
			Score.score+= 1;//adds score +1 for each kill
			Destroy (gameObject);
		}
	}
	//how much damage do we do from the enemy health
	public void HurtEnemy(int damage)
	{
		currentHealth -= damage;
	}

} 
