﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour {

	//to access the player health.
	private PlayerHealth player;

	// Use this for initialization
	void Start () {
		//gets the player script for the gameobject with the tag Player.
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
	}

	//this will be called when a collider enters the trigger
	void OnTriggerEnter2D(Collider2D col) 
	{
		//when somehing with the tag player collides with the enemy it's health will be reduced
		if (col.CompareTag ("Player")) 
		{	
			Destroy (gameObject);
			//ammount of dmage that the enemy will be doing to the player
			player.Damage (1);
		}
	} 


}
