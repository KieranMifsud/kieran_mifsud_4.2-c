﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour {

	public int timeLeft = 40;
	public Text countdownText;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine ("LoseTime");//time starts to decrase from the begnign of the level
	}
	
	// Update is called once per frame
	void Update () 
	{
		countdownText.text = ("Time Left = " + timeLeft); //shows the time left in the level
		if (timeLeft <= 0)//this will stop the timw at 0 
		{
			StopCoroutine ("LoseTime");
			WinScene ();
		}
	}

	void WinScene() 
	{
		Application.LoadLevel (4);//if player survuvous the entire time he goes to win scene
	}
	IEnumerator LoseTime()   
	{
		while (true) 
		{
			yield return new WaitForSeconds (1);//waits 1 second to reduce the time
			timeLeft--;//reduces time
		}
	}
}
