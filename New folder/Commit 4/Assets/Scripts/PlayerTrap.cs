﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTrap : MonoBehaviour {

	public static float multiplier = 15f;//by how much the trap will slow the player
	public float duration = 9f;//duration of trap
	private GameObject player;

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.CompareTag ("Player")) 
		{
			StartCoroutine( Pickup (other));//to pickup trap
		}
	}


	void Start()
	{
		player = GameObject.Find ("player");
	}

	IEnumerator Pickup(Collider2D player)
	{
		PlayerMovement speed = player.GetComponentInParent<PlayerMovement> ();
		speed.moveSpeed /= multiplier; //movement speed decreases by the multiplier

		//disable graphics so the player dosent collide with trap again
		GetComponent<SpriteRenderer> ().enabled = false;
		GetComponent<Collider2D> ().enabled = false;

		yield return new WaitForSeconds (duration);//duration of trap before returing to normal


		speed.moveSpeed *= multiplier;//gets player normal speed back

		Destroy (gameObject);//destrys trap

	}
}