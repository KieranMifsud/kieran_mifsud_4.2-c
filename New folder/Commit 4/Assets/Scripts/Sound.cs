﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sound : MonoBehaviour {

	public GameObject soundControlButton;
	public Sprite audioOffSprite;//mute button
	public Sprite audioOnSprite;//unmute button

	void Start() 
	{
		if (AudioListener.pause == true) {
			soundControlButton.GetComponent<Image> ().sprite = audioOffSprite;//sound is pasued
		} else {
			soundControlButton.GetComponent<Image> ().sprite = audioOnSprite;//sound is not paused
		}
	}

	public void SoundControl()
	{
		if (AudioListener.pause == true) {//sound is paused
			AudioListener.pause = false;//used to resune sound when pased
			soundControlButton.GetComponent<Image> ().sprite = audioOnSprite;
		} else {
			AudioListener.pause = true;
			soundControlButton.GetComponent<Image> ().sprite = audioOffSprite;
		}
	}
}