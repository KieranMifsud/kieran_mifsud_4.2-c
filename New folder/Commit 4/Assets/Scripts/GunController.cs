﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour {

	public bool isFiring;
	public Bullet_Control bullet;
	public float BulletSpeed;
	public float timeBetweenShots;//this will be use dto put a del between shots
	private float shotCounter;//countdown for the time between shots
	public Transform firePoint;//where the bullets will fire from
	public AudioSource gun_fireSound;


	// Update is called once per frame
	void Update ()
	{
		//setting variables for the fire rate of my layers
		if (isFiring) {
			shotCounter -= Time.deltaTime;
			if (shotCounter <= 0) //whenever shot counter goes below 0 another bullet can be fired
			{
				shotCounter = timeBetweenShots;
				//all the propberties of a bulley control that is needed
				Bullet_Control newBullet = Instantiate (bullet, firePoint.position, firePoint.rotation) as Bullet_Control;
				newBullet.speed = BulletSpeed;

				gun_fireSound.Play();//calling the gun fire sound
		}
		} else 
		{
			shotCounter = 0;//when user is not firing shot counter goes to 0 
			//so when he wants to fire a bullet it will fire immediatly
		}

	}
}