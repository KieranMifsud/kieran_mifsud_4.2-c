﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HBS : MonoBehaviour {
	
	//this is used to create an array to store the health sprites
	public Sprite[] HealthBarSprites; 

	//this is for the actual image that is on the screen
	public Image HealthBarUI;

	//to access the health of the player
	private PlayerHealth player;

	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerHealth> ();
	}


	// Update is called once per frame
	void Update () 
	{
		//this will make an image of the health bar come-up 
		HealthBarUI.sprite = HealthBarSprites[player.CurrentHealth];
	}
}
