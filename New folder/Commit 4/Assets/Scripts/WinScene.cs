﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinScene : MonoBehaviour {

	Text text;
	public void MainMenu()
	{
		Application.LoadLevel (0);//goes to main menu button is pressed
	}
	void Awake () {
		text = GetComponent<Text> ();

	}

	void Update (){
		text.text = "Your final score is : " + Score.score;//puts your score in the win scene
	}

	public void QuitGame()
	{
		UnityEditor.EditorApplication.isPlaying = false;//quits the game when quit button is pressed
	}
}
