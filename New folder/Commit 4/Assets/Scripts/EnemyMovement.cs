﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

	public float speed;//emeny speed

	private Transform target;//to follow player

	// Use this for initialization
	void Start () {
		//for the enemy to find his target
		target = GameObject.FindGameObjectWithTag ("Player").GetComponent<Transform> ();
	}

	// Update is called once per frame
	void Update () {
		//moves enemy from his position to the player posiiton at a certain speed
		transform.position = Vector2.MoveTowards (transform.position, target.position, speed * Time.deltaTime);
	}
}
