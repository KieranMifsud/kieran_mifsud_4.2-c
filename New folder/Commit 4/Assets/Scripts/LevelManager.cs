﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
	
	public void LoadLevel (string levelname)
	{
		SceneManager.LoadScene(levelname);//loads main level
	}

	public void QuitGame()
	{
		UnityEditor.EditorApplication.isPlaying = false;//quits game
	}

	public void LoadNextLevel()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);//loads instructions
	}

}
	