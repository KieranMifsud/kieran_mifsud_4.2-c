﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Control : MonoBehaviour {
	//this let the user control bullet speed
	public float speed = 8f;

	public int damageToGive;

	void Update () {
	//makes the bullet travel on one axis from fire point.
		transform.Translate(Vector3.up * speed * Time.deltaTime);
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Enemy") 
		{
			//get the component enemy health to give damage to the enemy
			other.gameObject.GetComponent<EnemyHealth>().HurtEnemy(damageToGive);
			Destroy (gameObject);
		}
	}
}
  