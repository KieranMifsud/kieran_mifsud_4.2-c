﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	public GameObject Enemy;
	float randY;
	float randX;
	Vector2 whereToSpawn;
	public float spawnTime;
	float nextSpawn = 0;

	
	// Update is called once per frame
	void Update () {
		if (Time.time > nextSpawn)//when  spawn time becomes 0 the next enemy will spawn
		{
			nextSpawn = Time.time + spawnTime;
			randY = Random.Range (-3.5f,3f);//emenies can spawn randomly bwtween these numbers
			randX = Random.Range (-7.5f, 8f);
		
			whereToSpawn = new Vector2 (randX, randY);
			Instantiate (Enemy, whereToSpawn, Quaternion.identity);
		}
	}
}
